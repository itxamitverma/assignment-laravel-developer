<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Symfony\Component\HttpFoundation\Response;

class CheckActiveSession
{
    /**
     * Handle an incoming request.
     *
     * @param  \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response)  $next
     */
    public function handle($request, Closure $next)
    {
        $ipAddress = $request->ip();

        $activeSession = Session::get('active_sessions')[$ipAddress] ?? null;

        if ($activeSession) {
            // Display prompt for another session in progress
            return redirect('/session-conflict')->with('message', 'Another session is already in progress.');
        }

        Session::put('active_sessions', [$ipAddress => true]);

        return $next($request);
    }
}
