<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Support\Facades\Auth;

class DashboardController extends Controller
{
    public function index()
    {
        $user = Auth::user();
        $greeting = User::getGreeting();
        return view('dashboard', compact('user', 'greeting'));
    }

    public function sessionConflict()
    {
        return view('session-conflict');
    }
}
