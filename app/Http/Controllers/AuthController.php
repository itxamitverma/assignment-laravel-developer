<?php

namespace App\Http\Controllers;

use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class AuthController extends Controller
{
    public function index(): View
    {
        return view('login');
    }

    public function login(Request $request)
    {
        $credentials = $request->only('email', 'password');

        if (Auth::attempt($credentials)) {

            return redirect()->intended('/dashboard');
        } else {
            // Authentication failed
            return back()->withErrors(['email' => 'Invalid email or password']);
        }
    }

    public function handleSecondSession(Request $request)
    {
        $userIdentifier = $request->ip();

        // Update new session information
        Session::put('active_sessions', [$userIdentifier => true]);

        // Invalidate previous session
        $previousSessionId = Session::get('active_sessions')[$userIdentifier];
        Session::invalidate($previousSessionId);

        $userIdentifier = $request->ip();
        Session::put('active_sessions', [$userIdentifier => true]);

        return redirect('/dashboard');
    }

    public function logout(Request $request)
    {
        Auth::logout();

        $request->session()->invalidate();

        $request->session()->regenerateToken();

        return redirect('/');
    }
}
