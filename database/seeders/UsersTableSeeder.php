<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run()
    {
         // Truncate the table before seeding
         DB::table('users')->truncate();

        // Example data to seed
        $users = [
            ['name' => 'John Doe', 'email_verified_at' => now(), 'email' => 'john@example.com', 'password' => bcrypt('Qawsed#57')],
            ['name' => 'Jane Doe',  'email_verified_at' => now(), 'email' => 'jane@example.com', 'password' => bcrypt('Qawsed#57')],
        ];

        // Insert the data into the database
        foreach ($users as $user) {
            User::create($user);
        }
    }
}
