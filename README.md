1. **Clone the Repository**: Open your terminal or command prompt, navigate to the directory where you want to clone the repository, and run the following command:

    ```
    git clone git@gitlab.com:itxamitverma/assignment-laravel-developer.git
    ```

2. **Navigate to the Project Directory**: After cloning the repository, navigate into the project directory using the `cd` command:

    ```
    cd assignment-laravel-developer
    ```

3. **Install Composer Dependencies**: Laravel projects require dependencies managed by Composer. Run the following command to install the dependencies:

    ```
    composer install
    ```

4. **Copy Environment File**: Laravel uses a `.env` file to manage environment-specific configurations. Copy the `.env.example` file to `.env`:

    ```
    cp .env.example .env
    ```

5. **Generate Application Key**: Laravel requires an application key to encrypt various elements within your application. Generate it using Artisan:

    ```
    php artisan key:generate
    ```

6. **Create Database**: Create a database for your Laravel application and update the `.env` file with your database credentials.

7. **Run Migrations (Optional)**: If the project has migrations for setting up the database schema, you can run them using Artisan:

    ```
    php artisan migrate
    ```


