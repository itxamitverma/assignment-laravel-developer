<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Laravel</title>

    <!-- Fonts -->
    <link rel="preconnect" href="https://fonts.bunny.net">
    <link href="https://fonts.bunny.net/css?family=figtree:400,600&display=swap" rel="stylesheet" />
    <script src="https://cdn.tailwindcss.com"></script>

</head>

<body class="antialiased">
    <div>
        @if (Route::has('login'))
            <div class="sm:fixed sm:top-0 sm:right-0 p-6 text-right z-10">
                @auth
                    <a href="{{ url('/home') }}"
                        class="font-semibold text-gray-600 hover:text-gray-900 dark:text-gray-400 dark:hover:text-white focus:outline focus:outline-2 focus:rounded-sm focus:outline-red-500">Home</a>
                @else
                    <a href="{{ route('login') }}"
                        class="font-semibold text-gray-600 hover:text-gray-900 dark:text-gray-400 dark:hover:text-white focus:outline focus:outline-2 focus:rounded-sm focus:outline-red-500">Log
                        in</a>

                    @if (Route::has('register'))
                        <a href="{{ route('register') }}"
                            class="ml-4 font-semibold text-gray-600 hover:text-gray-900 dark:text-gray-400 dark:hover:text-white focus:outline focus:outline-2 focus:rounded-sm focus:outline-red-500">Register</a>
                    @endif
                @endauth
            </div>
        @endif

        <section class="bg-gray-50 min-h-screen flex items-center justify-center">
            <!-- login container -->
            <div class="bg-gray-100 flex rounded-2xl shadow-lg max-w-3xl p-5 items-center">
                <!-- form -->
                <div class="md:w-1/2 px-8 md:px-16">
                    <h2 class="font-bold text-2xl text-[#002D74]">Login</h2>

                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                    <form action="{{ route('login.post') }}" method="POST" class="flex flex-col gap-4">
                        @csrf
                        <input id="email" class="p-2 mt-8 rounded-xl border" type="email" name="email"
                            placeholder="Email">
                        <div class="relative">
                            <input id="password" class="p-2 rounded-xl border w-full" type="password" name="password"
                                placeholder="Password">
                            <button type="button" onclick="togglePasswordVisibility()"
                                class="bi bi-eye absolute top-1/2 right-3 -translate-y-1/2">
                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16"
                                    fill="currentColor" class="bi bi-eye-slash" viewBox="0 0 16 16">
                                    <path
                                        d="M15.646 14.354l-14-14 .708-.708 14 14-.708.708zM8 9a2 2 0 1 0 0-4 2 2 0 0 0 0 4zm.01-7.986a18.022 18.022 0 0 0-6.7 3.183L3.192 5.19l1.418 1.418 1.115-1.115a13.98 13.98 0 0 1 5.09-2.062L8.01 1.014zM1.665 9.192L.247 7.774A13.97 13.97 0 0 1 1.986 5.7l1.115-1.115A15.97 15.97 0 0 0 .01 8.014L1.665 9.19zM8 11.986a18.022 18.022 0 0 0 6.7-3.183l1.418 1.418-1.418 1.418-1.115-1.115a13.98 13.98 0 0 1-5.09 2.062L8.01 13.014zm6.335-3.192l1.418-1.418a13.97 13.97 0 0 1-1.739 1.074L12.81 9.19a15.97 15.97 0 0 0 1.425-2.625zM8 1.014l-.115.68a13.98 13.98 0 0 1 5.09 2.062l1.115-1.115a15.97 15.97 0 0 0-2.625-1.425L8 1.014zm-.01 13.972l.115-.68a13.98 13.98 0 0 1-5.09-2.062L1.986 12.3a15.97 15.97 0 0 0 2.625 1.425l-.005.261zM3.192 3.192l8.617 8.617-1.418 1.418-8.617-8.617 1.418-1.418z" />
                                </svg>
                            </button>

                            <p>Email: john@example.com</p>
                            <p>Email: Qawsed#57</p>

                        </div>

                        <button
                            class="bg-[#002D74] rounded-xl text-white py-2 hover:scale-105 duration-300">Login</button>
                    </form>
                </div>

                <!-- image -->
                <div class="md:block hidden w-1/2">
                    <img class="rounded-2xl"
                        src="https://images.unsplash.com/photo-1616606103915-dea7be788566?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1887&q=80">
                </div>
            </div>
        </section>

    </div>
</body>

<script>
    function togglePasswordVisibility() {
        var passwordField = document.getElementById("password");
        var eyeIcon = document.querySelector(".bi-eye");
        if (passwordField.type === "password") {
            passwordField.type = "text";
            eyeIcon.innerHTML = `
                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-eye-slash" viewBox="0 0 16 16">
                    <path d="M15.646 14.354l-14-14 .708-.708 14 14-.708.708zM8 9a2 2 0 1 0 0-4 2 2 0 0 0 0 4zm.01-7.986a18.022 18.022 0 0 0-6.7 3.183L3.192 5.19l1.418 1.418 1.115-1.115a13.98 13.98 0 0 1 5.09-2.062L8.01 1.014zM1.665 9.192L.247 7.774A13.97 13.97 0 0 1 1.986 5.7l1.115-1.115A15.97 15.97 0 0 0 .01 8.014L1.665 9.19zM8 11.986a18.022 18.022 0 0 0 6.7-3.183l1.418 1.418-1.418 1.418-1.115-1.115a13.98 13.98 0 0 1-5.09 2.062l.115.68zm6.335-3.192l1.418-1.418a13.97 13.97 0 0 1-1.739 1.074L12.81 9.19a15.97 15.97 0 0 0 1.425-2.625zM8 1.014l-.115.68a13.98 13.98 0 0 1 5.09 2.062l1.115-1.115a15.97 15.97 0 0 0-2.625-1.425L8 1.014zm-.01 13.972l.115-.68a13.98 13.98 0 0 1-5.09-2.062L1.986 12.3a15.97 15.97 0 0 0 2.625 1.425l-.005.261zM3.192 3.192l8.617 8.617-1.418 1.418-8.617-8.617 1.418-1.418z"/>
                </svg>
            `;
        } else {
            passwordField.type = "password";
            eyeIcon.innerHTML = `
                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="gray" class="bi bi-eye" viewBox="0 0 16 16">
                    <path d="M16 8s-3-5.5-8-5.5S0 8 0 8s3 5.5 8 5.5S16 8 16 8zM1.173 8a13.133 13.133 0 0 1 1.66-2.043C4.12 4.668 5.88 3.5 8 3.5c2.12 0 3.879 1.168 5.168 2.457A13.133 13.133 0 0 1 14.828 8c-.058.087-.122.183-.195.288-.335.48-.83 1.12-1.465 1.755C11.879 11.332 10.119 12.5 8 12.5c-2.12 0-3.879-1.168-5.168-2.457A13.134 13.134 0 0 1 1.172 8z"/>
                    <path d="M8 5.5a2.5 2.5 0 1 0 0 5 2.5 2.5 0 0 0 0-5zM4.5 8a3.5 3.5 0 1 1 7 0 3.5 3.5 0 0 1-7 0z"/>
                </svg>
            `;
        }
    }
</script>

</html>
